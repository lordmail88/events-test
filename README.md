# Event Test

React test application to display GoogleCalendar API

## Demo Apps

[Demo Apps](https://events-test.toni-ismail.com/)
Notes: Please Allow popup to display google credential window

## Technology In Use

- React
- Redux
- React Redux
- Material UI
- Google API Client
  and many more (you can see in the package.json)

## Development

1. Clone this git
2. run `yarn install`
3. run `yarn start`
