/**
 *
 * BaseTemplate
 *
 */
import * as React from 'react';
import clsx from 'clsx';
import { useTranslation } from 'react-i18next';
import { messages } from './messages';
import { translations } from 'locales/translations';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import LanguageSharpIcon from '@material-ui/icons/LanguageSharp';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Flag from 'react-flagkit';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import ChevronLeftSharpIcon from '@material-ui/icons/ChevronLeftSharp';
import ChevronRightSharpIcon from '@material-ui/icons/ChevronRightSharp';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';
import AccountCircleSharpIcon from '@material-ui/icons/AccountCircleSharp';
import MomentUtils from '@date-io/moment';
import {
    DatePicker,
    MuiPickersUtilsProvider,
    Calendar,
} from '@material-ui/pickers';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import moment from 'moment';
import Grow from '@material-ui/core/Grow';
import { useDispatch, useSelector } from 'react-redux';
import {
    selectAvailableView,
    selectDate,
    selectSelectedView,
    selectSelectedViewObj,
} from 'app/pages/HomePage/slice/selectors';
import { useCalendarControlSlice } from 'app/pages/HomePage/slice';
import {
    selectCalendars,
    selectIsLoadingCalendar,
    selectIsSignIn,
    selectIsSignInLoading,
    selectProfileImageUrl,
    selectProfileName,
    selectSelectedCalendarIds,
    selectShouldRedirect,
} from 'app/pages/WelcomePage/slice/selectors';
import { useWelcomePageControlSlice } from '../../pages/WelcomePage/slice';
import { useEffect } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Redirect } from 'react-router-dom';
import TodaySharpIcon from '@material-ui/icons/TodaySharp';
import DateRangeSharpIcon from '@material-ui/icons/DateRangeSharp';
import ArrowDropDownSharpIcon from '@material-ui/icons/ArrowDropDownSharp';

interface Props {
    children: React.ReactNode;
}

const drawerWidth = 300;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    grow: {
        flexGrow: 1,
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9),
        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
}));

export function BaseTemplate(props: Props) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    // Used to dispatch slice actions

    const dispatch = useDispatch();
    const { t, i18n } = useTranslation();
    const classes = useStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
    const [open, setOpen] = React.useState(true);
    const handleDrawerOpen = () => {
        setOpen(true);
    };
    const handleDrawerClose = () => {
        setOpen(false);
    };

    const handleChangeLang = (lang: string) => {
        i18n.changeLanguage(lang);
    };

    const { actions } = useCalendarControlSlice();
    const selectedView = useSelector(selectSelectedView);
    const selectedViewObj = useSelector(selectSelectedViewObj);
    const availableViews = useSelector(selectAvailableView);
    const date = useSelector(selectDate);

    // Google api Reselect
    const profileName = useSelector(selectProfileName);
    const profileImageUrl = useSelector(selectProfileImageUrl);
    const isSigninLoading = useSelector(selectIsSignInLoading);
    const isLoadingCalendar = useSelector(selectIsLoadingCalendar);
    const shouldRedirect = useSelector(selectShouldRedirect);
    const calendars = useSelector(selectCalendars);
    const selectedCalendarIds = useSelector(selectSelectedCalendarIds);

    // Google api Actions
    const gapiAction = useWelcomePageControlSlice();

    useEffect(() => {
        dispatch(gapiAction.actions.checkCredential());
        dispatch(gapiAction.actions.getCalendars());
    }, [dispatch]);

    return (
        <div className={classes.root}>
            <AppBar position="absolute" className={classes.appBar}>
                <Toolbar className={classes.toolbar}>
                    <Typography
                        component="h1"
                        variant="h6"
                        color="inherit"
                        noWrap
                    >
                        {t(translations.appTitle)}
                    </Typography>

                    <Button
                        color="inherit"
                        style={{ marginLeft: 100 }}
                        onClick={() => {
                            dispatch(actions.todayView());
                        }}
                    >
                        Today
                    </Button>

                    <IconButton
                        color={'inherit'}
                        onClick={() => {
                            dispatch(actions.backwardView());
                        }}
                    >
                        <ChevronLeftSharpIcon />
                    </IconButton>
                    <IconButton
                        color={'inherit'}
                        onClick={() => {
                            dispatch(actions.forwardView());
                        }}
                    >
                        <ChevronRightSharpIcon />
                    </IconButton>
                    <Typography color="inherit" noWrap>
                        {moment(date).format(selectedViewObj.format)}
                    </Typography>

                    <div className={classes.grow}></div>

                    <PopupState variant="popover" popupId="popup-view-menu">
                        {popupState => (
                            <React.Fragment key={'view-menu'}>
                                <Button
                                    color="inherit"
                                    startIcon={<DateRangeSharpIcon />}
                                    endIcon={<ArrowDropDownSharpIcon />}
                                    {...bindTrigger(popupState)}
                                >
                                    {t(translations[`view_${selectedView}`])}
                                </Button>
                                <Menu {...bindMenu(popupState)}>
                                    {availableViews.map(aView => (
                                        <MenuItem
                                            key={aView.type}
                                            onClick={() => {
                                                popupState.close();
                                                dispatch(
                                                    actions.changeView(
                                                        aView.type,
                                                    ),
                                                );
                                            }}
                                            selected={
                                                aView.type == selectedView
                                            }
                                        >
                                            {t(
                                                translations[
                                                    `view_${aView.type}`
                                                ],
                                            )}
                                        </MenuItem>
                                    ))}
                                </Menu>
                            </React.Fragment>
                        )}
                    </PopupState>

                    <PopupState variant="popover" popupId="popup-lang-menu">
                        {popupState => (
                            <React.Fragment key={'lang-menu'}>
                                <Button
                                    color="inherit"
                                    startIcon={<LanguageSharpIcon />}
                                    {...bindTrigger(popupState)}
                                >
                                    {i18n.language.toUpperCase()}
                                </Button>
                                <Menu {...bindMenu(popupState)}>
                                    <MenuItem
                                        selected={
                                            i18n.language == 'en-US' ||
                                            i18n.language == 'en'
                                        }
                                        onClick={() => {
                                            handleChangeLang('en');
                                            popupState.close();
                                        }}
                                    >
                                        <ListItemIcon>
                                            <Flag country="US" />
                                        </ListItemIcon>{' '}
                                        <Typography variant="inherit">
                                            English
                                        </Typography>
                                    </MenuItem>
                                    <MenuItem
                                        selected={i18n.language == 'af'}
                                        onClick={() => {
                                            handleChangeLang('af');
                                            popupState.close();
                                        }}
                                    >
                                        <ListItemIcon>
                                            <Flag country="AF" />
                                        </ListItemIcon>{' '}
                                        <Typography variant="inherit">
                                            Afrikaans
                                        </Typography>
                                    </MenuItem>
                                    <MenuItem
                                        selected={i18n.language == 'id'}
                                        onClick={() => {
                                            handleChangeLang('id');
                                            popupState.close();
                                        }}
                                    >
                                        <ListItemIcon>
                                            <Flag country="ID" />
                                        </ListItemIcon>{' '}
                                        <Typography variant="inherit">
                                            Bahasa Indonesia
                                        </Typography>
                                    </MenuItem>
                                </Menu>
                            </React.Fragment>
                        )}
                    </PopupState>

                    {isSigninLoading ? (
                        <CircularProgress color={'secondary'} />
                    ) : (
                        <PopupState
                            variant="popover"
                            popupId="popup-profile-menu"
                        >
                            {popupState => (
                                <React.Fragment key={'profile-menu'}>
                                    <Button
                                        color="inherit"
                                        startIcon={
                                            <Avatar
                                                alt={profileName}
                                                src={profileImageUrl}
                                            />
                                        }
                                        {...bindTrigger(popupState)}
                                    >
                                        {profileName}
                                    </Button>
                                    <Menu {...bindMenu(popupState)}>
                                        <MenuItem
                                            onClick={() => {
                                                popupState.close();
                                                dispatch(
                                                    gapiAction.actions.signOut(),
                                                );
                                            }}
                                        >
                                            Logout
                                        </MenuItem>
                                    </Menu>
                                </React.Fragment>
                            )}
                        </PopupState>
                    )}
                </Toolbar>
            </AppBar>
            <Drawer
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper,
                }}
                open={open}
            >
                <div className={classes.toolbarIcon}>
                    <IconButton onClick={handleDrawerClose}>
                        <ChevronLeftIcon />
                    </IconButton>
                </div>
                <Divider />
                <Grow in={true}>
                    <Paper style={{ overflow: 'hidden' }}>
                        <MuiPickersUtilsProvider utils={MomentUtils}>
                            <Calendar date={moment(date)} onChange={() => {}} />
                        </MuiPickersUtilsProvider>
                    </Paper>
                </Grow>

                <Box mt={4}>
                    <Grow in={true}>
                        <Card>
                            <CardHeader title={t(translations.calendar_list)} />
                            {isLoadingCalendar ? (
                                <CircularProgress />
                            ) : (
                                <List dense>
                                    {calendars.map(cal => {
                                        const labelId = `checkbox-list-label-${cal.id}`;
                                        return (
                                            <ListItem
                                                key={cal.id}
                                                role={undefined}
                                                dense
                                                button
                                            >
                                                <ListItemIcon>
                                                    <Checkbox
                                                        edge="start"
                                                        checked={selectedCalendarIds.some(
                                                            x => cal.id == x,
                                                        )}
                                                        tabIndex={-1}
                                                        disableRipple
                                                        inputProps={{
                                                            'aria-labelledby': labelId,
                                                        }}
                                                    />
                                                </ListItemIcon>
                                                <ListItemText
                                                    id={labelId}
                                                    primary={cal.summary}
                                                    secondary={
                                                        <React.Fragment>
                                                            <Typography
                                                                component="span"
                                                                variant="body2"
                                                                color="textSecondary"
                                                                style={{
                                                                    display:
                                                                        'block',
                                                                }}
                                                                noWrap
                                                            >
                                                                {
                                                                    cal.description
                                                                }
                                                            </Typography>
                                                        </React.Fragment>
                                                    }
                                                />
                                            </ListItem>
                                        );
                                    })}
                                </List>
                            )}
                        </Card>
                    </Grow>
                </Box>
            </Drawer>
            <main className={classes.content}>
                <div className={classes.appBarSpacer} />
                <Container maxWidth="lg" className={classes.container}>
                    <div>{props.children}</div>
                </Container>
            </main>

            {shouldRedirect && <Redirect to="/" />}
        </div>
    );
}
