/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import { HomePage } from './pages/HomePage/Loadable';
import { WelcomePage } from './pages/WelcomePage/Loadable';
import { NotFoundPage } from './components/NotFoundPage/Loadable';
import { useTranslation } from 'react-i18next';
import { BaseTemplate } from './components/BaseTemplate';
import moment from 'moment';

export function App() {
    const { i18n } = useTranslation();
    const momentLocaleFromi18n =
        i18n.language == 'en' ? 'en-gb' : i18n.language;

    // set default moment locale
    moment.locale(momentLocaleFromi18n);

    return (
        <BrowserRouter>
            <Helmet
                titleTemplate="%s - Events Test"
                defaultTitle="Event Test"
                htmlAttributes={{ lang: i18n.language }}
            >
                <meta
                    name="description"
                    content="A React Application for CodelabProjects Test"
                />
            </Helmet>

            <Switch>
                <Route exact path="/" component={WelcomePage} />
                <BaseTemplate>
                    <Route path="/app" component={HomePage} />
                </BaseTemplate>

                <Route component={NotFoundPage} />
            </Switch>
        </BrowserRouter>
    );
}
