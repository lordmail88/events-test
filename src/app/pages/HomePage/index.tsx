import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { Calendar, momentLocalizer, Views } from 'react-big-calendar';
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { useSelector, useDispatch } from 'react-redux';
import { useCalendarControlSlice } from './slice';
import { selectDate, selectSelectedView, selectToday } from './slice/selectors';
import { selectEvents } from 'app/pages/WelcomePage/slice/selectors';
import Grid from '@material-ui/core/Grid';
import {
    DatePicker,
    MuiPickersUtilsProvider,
    Calendar as SmallCalendar,
} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { useTranslation } from 'react-i18next';
import Grow from '@material-ui/core/Grow';
import { gapi } from 'gapi-script';
import { useEffect } from 'react';

const localizer = momentLocalizer(moment);
let allViews = Object.keys(Views).map(k => Views[k]);

export function HomePage() {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars

    // react-i18next for localization
    const { t, i18n } = useTranslation();

    // Use the slice we created
    const { actions } = useCalendarControlSlice();

    // Used to dispatch slice actions
    const dispatch = useDispatch();

    // `selectors` are used to read the state. Explained in other chapter
    // Will be inferred as `string` type ✅
    const date = useSelector(selectDate);
    const today = useSelector(selectToday);
    const selectedView = useSelector(selectSelectedView);
    const events = useSelector(selectEvents);

    return (
        <>
            <Helmet>
                <title>Home Page</title>
                <meta
                    name="description"
                    content="A Boilerplate application homepage"
                />
            </Helmet>

            <Grow in={true}>
                <Paper>
                    <Calendar
                        date={moment(date).toDate()}
                        toolbar={false}
                        views={allViews}
                        view={selectedView}
                        step={60}
                        events={events.map(x => {
                            return {
                                title: x.title,
                                start: moment(x.start).toDate(),
                                end: moment(x.end).toDate(),
                                allDay: x.allDay,
                                resource: x.resource,
                            };
                        })}
                        onNavigate={(a, b, c) => {
                            if (c == 'DATE') {
                                dispatch(
                                    actions.selectDate(moment(a).valueOf()),
                                );
                                dispatch(actions.changeView('day'));
                            }
                        }}
                        onView={(a, b, c) => {
                            console.log(a, b, c);
                        }}
                        showMultiDayTimes
                        localizer={localizer}
                        startAccessor="start"
                        endAccessor="end"
                        style={{ height: 500 }}
                    />
                </Paper>
            </Grow>
        </>
    );
}
