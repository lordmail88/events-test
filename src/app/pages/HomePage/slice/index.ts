import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { calendarControlSaga } from './saga';
import { CalendarControlState } from './types';
import moment, { DurationInputArg2 } from 'moment';

export const initialState: CalendarControlState = {
    date: Date.now(),
    today: Date.now(),
    views: [
        {
            type: 'month',
            format: 'MMM YYYY',
            momentUnit: 'M',
        },
        {
            type: 'week',
            format: 'Wo, YYYY',
            momentUnit: 'w',
        },
        {
            type: 'day',
            format: 'DD MMM YYYY',
            momentUnit: 'd',
        },
        {
            type: 'agenda',
            format: 'DD MMM YYYY',
            momentUnit: 'd',
        },
    ],
    selectedView: 'month',
    selectedViewObj: {
        type: 'month',
        format: 'MMM YYYY',
        momentUnit: 'M',
    },
};

const slice = createSlice({
    name: 'calendarControl',
    initialState,
    reducers: {
        someAction(state, action: PayloadAction<any>) {},
        changeView(state, action: PayloadAction<string>) {
            state.selectedView = action.payload;
            state.selectedViewObj = state.views.find(
                x => x.type == action.payload,
            ) || {
                type: 'month',
                format: 'MMM YYYY',
                momentUnit: 'M',
            };
        },
        todayView(state) {
            state.date = moment().valueOf();
        },
        forwardView(state) {
            state.date = moment(state.date)
                .add(1, state.selectedViewObj.momentUnit)
                .valueOf();
        },
        backwardView(state) {
            state.date = moment(state.date)
                .subtract(1, state.selectedViewObj.momentUnit)
                .valueOf();
        },
        selectDate(state, action: PayloadAction<number>) {
            state.date = action.payload;
        },
    },
});

export const { actions: calendarControlActions } = slice;

export const useCalendarControlSlice = () => {
    useInjectReducer({ key: slice.name, reducer: slice.reducer });
    useInjectSaga({ key: slice.name, saga: calendarControlSaga });
    return { actions: slice.actions };
};

/**
 * Example Usage:
 *
 * export function MyComponentNeedingThisSlice() {
 *  const { actions } = useCalendarControlSlice();
 *
 *  const onButtonClick = (evt) => {
 *    dispatch(actions.someAction());
 *   };
 * }
 */
