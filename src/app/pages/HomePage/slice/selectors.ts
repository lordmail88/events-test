import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.calendarControl || initialState;

export const selectCalendarControl = createSelector(
  [selectSlice],
  state => state,
);

export const selectDate = createSelector([selectSlice], state => state.date);
export const selectToday = createSelector([selectSlice], state => state.today);
export const selectSelectedView = createSelector(
  [selectSlice],
  state => state.selectedView,
);
export const selectSelectedViewObj = createSelector(
  [selectSlice],
  state => state.selectedViewObj,
);
export const selectAvailableView = createSelector(
  [selectSlice],
  state => state.views,
);
