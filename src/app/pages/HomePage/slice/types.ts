/* --- STATE --- */
import { DurationInputArg2 } from 'moment';

export interface ViewType {
  type: string;
  format: string;
  momentUnit: DurationInputArg2;
}
export interface CalendarControlState {
  date: number;
  today: number;
  views: Array<ViewType>;
  selectedView: string;
  selectedViewObj: ViewType;
}
