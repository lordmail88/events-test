/**
 *
 * WelcomePage
 *
 */
import React, { memo, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { messages } from './messages';
import { makeStyles } from '@material-ui/core/styles';
import { gapi } from 'gapi-script';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import Link from '@material-ui/core/Link';
import Divider from '@material-ui/core/Divider';
import Slide from '@material-ui/core/Slide';
import Grow from '@material-ui/core/Grow';

import { translations } from 'locales/translations';
import { useSelector, useDispatch } from 'react-redux';
import { useWelcomePageControlSlice } from './slice';
import {
    selectIsSignIn,
    selectIsSignInLoading,
    selectProfileName,
} from './slice/selectors';

interface Props {}

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        whiteSpace: 'pre-line',
    },
    hero: {
        width: '100%',
        height: 200,
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,
        borderRadius: '0 0 50% 50%',
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    grow: {
        flexGrow: 1,
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
}));

const Copyright = () => {
    return (
        <div>
            <Divider variant="middle" style={{ marginBottom: 20 }} />
            <Typography variant="body2" color="textSecondary" align="center">
                {'Copyright © '}
                <Link color="inherit" href=".">
                    Event Test
                </Link>{' '}
                {new Date().getFullYear()}
                {'.'}
            </Typography>
        </div>
    );
};

export const WelcomePage = memo((props: Props) => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars

    // react-i18next for localization
    const { t, i18n } = useTranslation();

    // Use the slice we created
    const { actions } = useWelcomePageControlSlice();

    // Used to dispatch slice actions
    const dispatch = useDispatch();
    const classes = useStyles();

    // reselect
    const isSignInLoading = useSelector(selectIsSignInLoading);
    const isSignIn = useSelector(selectIsSignIn);
    const profileName = useSelector(selectProfileName);

    // Sign in Button Click Handler
    const signInClickHandler = () => {
        dispatch(actions.signIn());
    };

    useEffect(() => {});

    return (
        <div className={classes.root}>
            <main className={classes.content}>
                <Slide mountOnEnter unmountOnExit direction="down" in={true}>
                    <Paper elevation={3} className={classes.hero}>
                        <Box
                            height={200}
                            display={'flex'}
                            alignItems={'center'}
                            justifyContent={'center'}
                        >
                            <Box letterSpacing={4} m={1} display={'block'}>
                                <Typography variant={'h3'} component="h1">
                                    {t(translations.appTitle)}
                                </Typography>
                                <Typography
                                    align={'center'}
                                    variant="subtitle2"
                                    component={'div'}
                                    gutterBottom
                                >
                                    {t(translations.appSubTitle)}
                                </Typography>
                            </Box>
                        </Box>
                    </Paper>
                </Slide>

                <Container maxWidth="lg" className={classes.container}>
                    <Grow in={true} timeout={1000}>
                        <Box
                            justifyContent={'center'}
                            mt={3}
                            alignItems={'center'}
                        >
                            <Box display={'block'}>
                                <Typography
                                    align={'center'}
                                    variant="body1"
                                    gutterBottom
                                >
                                    {t(translations.welcome)}
                                </Typography>

                                <Typography
                                    align={'center'}
                                    variant="caption"
                                    component={'div'}
                                    gutterBottom
                                >
                                    {t(translations.welcomeNotes)}
                                </Typography>
                            </Box>

                            <Box
                                display="flex"
                                justifyContent={'center'}
                                m={3}
                                p={3}
                                alignItems={'center'}
                            >
                                {!isSignInLoading && !isSignIn && (
                                    <Button
                                        color={'primary'}
                                        variant="contained"
                                        onClick={signInClickHandler}
                                    >
                                        {t(translations.signIn)}
                                    </Button>
                                )}

                                {isSignInLoading && <CircularProgress />}

                                {isSignIn && (
                                    <div>
                                        <Typography
                                            variant="subtitle2"
                                            component={'div'}
                                            gutterBottom
                                        >
                                            Welcome, {profileName}
                                        </Typography>
                                        <Button
                                            color={'primary'}
                                            href={'/app'}
                                            variant="contained"
                                        >
                                            GoTo Your Google Calendar
                                        </Button>
                                    </div>
                                )}
                            </Box>
                        </Box>
                    </Grow>
                </Container>

                <footer>
                    <Copyright />
                </footer>
            </main>
        </div>
    );
});
