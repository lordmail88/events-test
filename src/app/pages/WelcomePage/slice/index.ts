import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { welcomePageControlSaga } from './saga';
import {
    GoogleCalendar,
    GoogleEvent,
    GoogleProfile,
    ReactBigCalendarEvent,
    WelcomePageControlState,
} from './types';
import moment from 'moment';

export const initialState: WelcomePageControlState = {
    isLoadingCalendar: false,
    calendars: [],
    selectedCalendarIds: [],
    isSignIn: false,
    profileEmail: '',
    profileId: '',
    profileName: '',
    isSignInLoading: false,
    shouldRedirect: false,
    profileImageUrl: '',
    isLoadingEvents: false,
    events: [],
};

const slice = createSlice({
    name: 'welcomePageControl',
    initialState,
    reducers: {
        someAction(state, action: PayloadAction<any>) {},
        signIn(state) {
            state.isSignInLoading = true;
            state.profileId = '';
            state.profileName = '';
            state.profileEmail = '';
            state.profileImageUrl = '';
        },
        signInLoaded(state, action: PayloadAction<GoogleProfile>) {
            state.isSignInLoading = false;
            state.isSignIn = true;
            state.profileId = action.payload.profileId;
            state.profileName = action.payload.profileName;
            state.profileEmail = action.payload.profileEmail;
            state.profileImageUrl = action.payload.profileImageUrl;
        },
        signInError(state, action: PayloadAction<string>) {
            state.isSignInLoading = false;
            state.isSignIn = false;
            state.profileId = '';
            state.profileName = '';
            state.profileEmail = '';
            state.profileImageUrl = '';
        },
        checkCredential(state) {
            state.isSignInLoading = true;
            state.profileId = '';
            state.profileName = '';
            state.profileEmail = '';
            state.profileImageUrl = '';
        },
        checkCredentialLoaded(state, action: PayloadAction<GoogleProfile>) {
            state.isSignInLoading = false;
            state.isSignIn = true;
            state.profileId = action.payload.profileId;
            state.profileName = action.payload.profileName;
            state.profileEmail = action.payload.profileEmail;
            state.profileImageUrl = action.payload.profileImageUrl;
        },
        checkCredentialError(state, action: PayloadAction<string>) {
            state.isSignInLoading = false;
            state.isSignIn = false;
            state.profileId = '';
            state.profileName = '';
            state.profileEmail = '';
            state.profileImageUrl = '';
        },
        signOut(state) {
            state.shouldRedirect = false;
        },
        signOutLoaded(state) {
            state.isSignInLoading = false;
            state.isSignIn = false;
            state.profileId = '';
            state.profileName = '';
            state.profileEmail = '';
            state.profileImageUrl = '';
            state.shouldRedirect = true;
        },
        getCalendars(state) {
            state.calendars = [];
            state.selectedCalendarIds = [];
            state.isLoadingCalendar = true;
        },
        getCalendarsLoaded(
            state,
            action: PayloadAction<Array<GoogleCalendar>>,
        ) {
            state.calendars = action.payload;
            state.selectedCalendarIds = [...action.payload.map(x => x.id)];
            state.isLoadingCalendar = false;
        },
        getCalendarsError(state) {
            state.calendars = [];
            state.selectedCalendarIds = [];
            state.isLoadingCalendar = true;
        },
        getEvents(state) {
            state.isLoadingEvents = true;
            state.events = [];
        },
        getEventsLoaded(state, action: PayloadAction<Array<GoogleEvent>>) {
            state.isLoadingEvents = false;
            state.events = action.payload.map(x => {
                return {
                    title: x.summary,
                    start: x.start.dateTime
                        ? moment(x.start.dateTime).valueOf()
                        : moment(x.start.date).valueOf(),
                    end: x.end.dateTime
                        ? moment(x.end.dateTime).valueOf()
                        : moment(x.end.date).valueOf(),
                    allDay: x.start.dateTime ? false : true,
                    resource: x,
                } as ReactBigCalendarEvent;
            }) as Array<ReactBigCalendarEvent>;
        },
        getEventsError() {},
    },
});

export const { actions: welcomePageControlActions } = slice;

export const useWelcomePageControlSlice = () => {
    useInjectReducer({ key: slice.name, reducer: slice.reducer });
    useInjectSaga({ key: slice.name, saga: welcomePageControlSaga });
    return { actions: slice.actions };
};

/**
 * Example Usage:
 *
 * export function MyComponentNeedingThisSlice() {
 *  const { actions } = useWelcomePageControlSlice();
 *
 *  const onButtonClick = (evt) => {
 *    dispatch(actions.someAction());
 *   };
 * }
 */
