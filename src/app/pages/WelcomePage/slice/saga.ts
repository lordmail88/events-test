import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { welcomePageControlActions as actions } from '.';
import {
    gapi,
    loadAuth2,
    loadClientAuth2,
    loadAuth2WithProps,
    loadGapiInsideDOM,
} from 'gapi-script';
import {
    GoogleApiResponse,
    GoogleCalendar,
    GoogleEvent,
    GoogleProfile,
} from './types';
import { selectSelectedCalendarIds } from './selectors';
import { getI18n } from 'react-i18next';

const GOOGLE_CLIENT_ID =
    '442339495736-3h30r2898fd0psgdu96hojfsrbk8ok1r.apps.googleusercontent.com';
const GOOGLE_API_KEY = 'AIzaSyCKpz_YDT5-mlCBb4iyqqXW-pgC2fvI8Jc';
const GOOGLE_CLIENT_INIT = {
    apiKey: GOOGLE_API_KEY,
    clientId: GOOGLE_CLIENT_ID,
    scope: 'https://www.googleapis.com/auth/calendar.readonly',
    discoveryDocs: [
        'https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest',
    ],
};

function* checkCredential() {
    try {
        const auth2 = yield call(
            loadAuth2,
            gapi,
            GOOGLE_CLIENT_ID,
            'https://www.googleapis.com/auth/calendar.readonly',
        );
        if (auth2.isSignedIn.get()) {
            const profile = gapi.auth2
                .getAuthInstance()
                .currentUser.get()
                .getBasicProfile();
            yield put(
                actions.checkCredentialLoaded({
                    profileEmail: profile.getEmail(),
                    profileName: profile.getName(),
                    profileId: profile.getId(),
                    profileImageUrl: profile.getImageUrl(),
                } as GoogleProfile),
            );
        } else {
            //yield put(actions.checkCredentialError(''));
            yield call(signOut);
        }
    } catch (e) {
        console.log(e);
        yield put(actions.signInError(e));
    }
}

function* googleSignIn() {
    try {
        const auth2Check = yield call(
            loadAuth2,
            gapi,
            GOOGLE_CLIENT_ID,
            'https://www.googleapis.com/auth/calendar.readonly',
        );
        if (auth2Check.isSignedIn.get()) {
            const profile = gapi.auth2
                .getAuthInstance()
                .currentUser.get()
                .getBasicProfile();
            yield put(
                actions.signInLoaded({
                    profileEmail: profile.getEmail(),
                    profileName: profile.getName(),
                    profileId: profile.getId(),
                } as GoogleProfile),
            );
        } else {
            const auth2 = gapi.auth2.getAuthInstance();
            yield call(auth2.signIn);
            yield call(checkCredential);
            // auth2Check.signIn().then((resp) => console.log(resp));
            //yield put(actions.signInError(''));
        }
    } catch (e) {
        console.log(e);
        yield put(actions.signInError(e));
    }
}

function* signOut() {
    const auth2 = gapi.auth2.getAuthInstance();
    yield call(auth2.signOut);
    yield put(actions.signOutLoaded());
}

function* gapiCalendars() {
    const auth2 = yield call(
        loadClientAuth2,
        gapi,
        GOOGLE_CLIENT_ID,
        'https://www.googleapis.com/auth/calendar.readonly',
    );
    yield call(gapi.client.init, GOOGLE_CLIENT_INIT);
    const cals: GoogleApiResponse = yield call(
        gapi.client.calendar.calendarList.list,
    );

    if (cals != null && cals.status == 200) {
        yield put(
            actions.getCalendarsLoaded(
                cals.result.items as Array<GoogleCalendar>,
            ),
        );

        yield call(gapiEvents);
    } else {
        yield put(actions.getCalendarsError());
    }
}

function* gapiEvents() {
    const auth2 = yield call(
        loadClientAuth2,
        gapi,
        GOOGLE_CLIENT_ID,
        'https://www.googleapis.com/auth/calendar.readonly',
    );
    yield call(gapi.client.init, GOOGLE_CLIENT_INIT);

    const calendarIds: Array<string> = yield select(selectSelectedCalendarIds);
    let allEvents: Array<GoogleEvent> = [];
    for (let calIdsIdx = 0; calIdsIdx < calendarIds.length; calIdsIdx++) {
        const events: GoogleApiResponse = yield call(
            gapi.client.calendar.events.list,
            {
                calendarId: calendarIds[calIdsIdx],
            },
        );
        if (events != null && events.status == 200) {
            allEvents = [
                ...allEvents,
                ...(events.result.items as Array<GoogleEvent>),
            ];
        }
    }
    yield put(actions.getEventsLoaded(allEvents as Array<GoogleEvent>));
}

export function* welcomePageControlSaga() {
    yield takeLatest(actions.signIn.type, googleSignIn);
    yield takeLatest(actions.checkCredential.type, checkCredential);
    yield takeLatest(actions.signOut.type, signOut);
    yield takeLatest(actions.getCalendars.type, gapiCalendars);
    yield takeLatest(actions.getEvents.type, gapiEvents);
}
