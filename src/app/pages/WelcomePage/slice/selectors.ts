import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) =>
    state.welcomePageControl || initialState;

export const selectWelcomePageControl = createSelector(
    [selectSlice],
    state => state,
);

export const selectIsSignInLoading = createSelector(
    [selectSlice],
    state => state.isSignInLoading,
);
export const selectIsLoadingCalendar = createSelector(
    [selectSlice],
    state => state.isLoadingCalendar,
);
export const selectIsSignIn = createSelector(
    [selectSlice],
    state => state.isSignIn,
);
export const selectProfileName = createSelector(
    [selectSlice],
    state => state.profileName,
);
export const selectProfileImageUrl = createSelector(
    [selectSlice],
    state => state.profileImageUrl,
);
export const selectShouldRedirect = createSelector(
    [selectSlice],
    state => state.shouldRedirect,
);

export const selectCalendars = createSelector(
    [selectSlice],
    state => state.calendars,
);
export const selectSelectedCalendarIds = createSelector(
    [selectSlice],
    state => state.selectedCalendarIds,
);
export const selectIsLoadingEvents = createSelector(
    [selectSlice],
    state => state.isLoadingEvents,
);
export const selectEvents = createSelector(
    [selectSlice],
    state => state.events,
);
