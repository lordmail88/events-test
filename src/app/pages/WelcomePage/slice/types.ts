/* --- STATE --- */

export interface GoogleProfile {
    profileId: string;
    profileEmail: string;
    profileName: string;
    profileImageUrl: string;
}

export interface DefaultReminder {
    method: string;
    minutes: number;
}

export interface Notification {
    type: string;
    method: string;
}

export interface NotificationSettings {
    notifications: Notification[];
}

export interface ConferenceProperties {
    allowedConferenceSolutionTypes: string[];
}

export interface GoogleCalendar {
    kind: string;
    etag: string;
    id: string;
    summary: string;
    timeZone: string;
    colorId: string;
    backgroundColor: string;
    foregroundColor: string;
    selected: boolean;
    accessRole: string;
    defaultReminders: DefaultReminder[];
    notificationSettings: NotificationSettings;
    primary: boolean;
    conferenceProperties: ConferenceProperties;
    description: string;
}

export interface GoogleApiResponseResult {
    etag: string;
    items: Array<GoogleCalendar> | Array<GoogleEvent>;
    kind: string;
    nextSyncToken: string;
}

export interface GoogleApiResponse {
    body: string;
    result: GoogleApiResponseResult;
    status: number;
}

export interface ReactBigCalendarEvent {
    title: string;
    start: number;
    end: number;
    allDay?: boolean;
    resource: GoogleEvent;
}

export interface GooglePerson {
    email: string;
    displayName: string;
    self: boolean;
}

export interface Start {
    date?: string;
    dateTime?: string;
}

export interface End {
    date?: string;
    dateTime?: string;
}

export interface GoogleEvent {
    kind: string;
    etag: string;
    id: string;
    status: string;
    htmlLink: string;
    created: Date;
    updated: Date;
    summary: string;
    creator: GooglePerson;
    organizer: GooglePerson;
    start: Start;
    end: End;
    transparency: string;
    visibility: string;
    iCalUID: string;
    sequence: number;
    eventType: string;
}
export interface WelcomePageControlState {
    isSignInLoading: boolean;
    isSignIn: boolean;
    profileId: string;
    profileEmail: string;
    profileName: string;
    profileImageUrl: string;
    shouldRedirect: boolean;
    selectedCalendarIds: Array<string>;
    calendars: Array<GoogleCalendar>;
    isLoadingCalendar: boolean;
    events: Array<ReactBigCalendarEvent>;
    isLoadingEvents: boolean;
}
