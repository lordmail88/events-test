/**
 * index.tsx
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

// Use consistent styling
import 'sanitize.css/sanitize.css';
import CssBaseline from '@material-ui/core/CssBaseline';
// Import root app
import { App } from 'app';

import { HelmetProvider } from 'react-helmet-async';

import { configureAppStore } from 'store/configureStore';

import reportWebVitals from 'reportWebVitals';

// Init moment languages
import 'moment/locale/en-gb';
import 'moment/locale/id';
import 'moment/locale/af';
// Initialize languages
import './locales/i18n';

import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import green from '@material-ui/core/colors/green';
import blue from '@material-ui/core/colors/blue';

const store = configureAppStore();
const MOUNT_NODE = document.getElementById('root') as HTMLElement;

const theme = createMuiTheme({
    palette: {
        primary: {
            main: blue[500],
        },
        secondary: {
            main: blue['A400'],
        },
    },
});

ReactDOM.render(
    <Provider store={store}>
        <HelmetProvider>
            <CssBaseline />
            <ThemeProvider theme={theme}>
                <App />
            </ThemeProvider>
        </HelmetProvider>
    </Provider>,
    MOUNT_NODE,
);

// Hot reloadable translation json files
if (module.hot) {
    module.hot.accept(['./locales/i18n'], () => {
        // No need to render the App again because i18next works with the hooks
    });
}

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
