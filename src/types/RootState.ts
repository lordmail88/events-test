import { CalendarControlState } from 'app/pages/HomePage/slice/types';
import { WelcomePageControlState } from 'app/pages/WelcomePage/slice/types';
// [IMPORT NEW CONTAINERSTATE ABOVE] < Needed for generating containers seamlessly

/* 
  Because the redux-injectors injects your reducers asynchronously somewhere in your code
  You have to declare them here manually
*/
export interface RootState {
  calendarControl?: CalendarControlState;
  welcomePageControl?: WelcomePageControlState;
  // [INSERT NEW REDUCER KEY ABOVE] < Needed for generating containers seamlessly
}
